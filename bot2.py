import discord
import secrets
import re
import requests

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

def is_valid_tmdb_link(link):
    # Utilise une expression régulière pour vérifier si le lien commence par https://www.themoviedb.org/
    return re.match(r'^https://www.themoviedb.org/', link) is not None


@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')

@client.event
async def on_message(message):
    if message.author == client.user:
        return




    if message.content.startswith('/register'):

        if message.guild:
            user = message.author
            username = user.name
            user_id = user.id
            user_avatar = user.display_avatar.with_size(128).url

            verified_role = discord.utils.get(user.roles, name='vérifié')
            if verified_role:
                roles = [role.name for role in user.roles]
                token = secrets.token_urlsafe(16)

                data = {"username": username, "id": user_id, "avatar": user_avatar, "token": token}
                print(data)
                response = requests.post("http://127.0.0.1:8000/registerdiscord/", json=data)
                print(response.status_code)

                await message.channel.send(f'>>> ** Bonjour {username}, Un DM vous a été envoyé **\n')
                if response.ok:
                    url = f'127.0.0.1:8000/login/'

                    # Crée une nouvelle vue avec un bouton
                    class MyView(discord.ui.View):
                        def __init__(self):
                            super().__init__(timeout=30)  # times out after 30 seconds
                            button = discord.ui.Button(label='Se connecter', style=discord.ButtonStyle.url,
                                                       url='http://127.0.0.1:8000/login/')
                            self.add_item(button)

                            async def on_timeout(self):
                                await self.message.edit(content="Link timed out", view=None)
                    # Envoie le message avec la vue
                    await message.author.send(
                        f' >>> **Bonjour {username}, Votre compte a bien été créer et vous pouvez vous connecter! utilisez :\n**'
                        f'||{token}||\n'
                        f'**comme mpd temporaire**\n',
                        view=MyView())
                elif response.status_code == 409:
                    await message.author.send(
                        f'### Une erreur est survenue: Vous avez déja créer votre compte. ###\n')
                else:
                    await message.author.send(
                        f' >>> ** Une erreur est survenu lors de la création de compte **')
            else:
                await message.channel.send("**Vous n'avez pas le rôle nécessaire.**")

        else:
            await message.channel.send(f'**tu ne peux utiliser cette commande que sur le serveur**')




    elif message.content.startswith('/requete') or message.content.startswith('/req'):
        if message.guild:
            user = message.author
            verified_role = discord.utils.get(user.roles, name='vérifié')
            if verified_role:

                content_parts = message.content.split()
                if len(content_parts) > 1:
                    if is_valid_tmdb_link(content_parts[1]):

                        username = user.name
                        tmdblink = content_parts[1]
                        data = {"tmdblink": tmdblink, "username": username}

                        response = requests.post("http://127.0.0.1:8000/requetedisord/", json=data)
                        print(response.status_code)

                        if response.ok:
                            await message.channel.send(
                                f' >>> **Bonjour {username}, Votre requête a bien été pris en compte**')
                        elif response.status_code == 404:
                            await message.channel.send(
                                f'### Une erreur est survenue: Vous ne possédez pas de compte sur le site. ###\n')
                        elif response.status_code == 409:
                            await message.channel.send(
                                f'### Une erreur est survenue: Une requête similaire existe déjà. ###\n')
                        else:
                            await message.channel.send(
                                f'### Une erreur est survenue lors de l\'ajout de ta requête ###\n')

                    else:
                        await message.channel.send(f'### il y une erreur dans ta requete, ###\n'
                                                   f'### Envoie un lien [tmdb](https://www.themoviedb.org/): ``` /requete https://www.themoviedb.org/... ``` ')
                else:
                    await message.channel.send(

                        f'### Quel film ou série désires-tu? ###\n'
                        f'### Envoie un lien [tmdb](https://www.themoviedb.org/): ``` /requete https://www.themoviedb.org/... ``` ')
        else:
            await message.channel.send(f'**tu ne peux utiliser cette commande que sur le serveur**')





    elif message.content.startswith('/help') or message.content.startswith('/h'):
        formatted_message = (
            f'>>> **S\'inscrire sur le site: (Vous devez avoir le rôle vérifié pour créer cotre compte)** \n'
            f'``` /register ```\n'

            f'**Demander à ajouté un film ou une série: (Vous devez avoir le rôle vérifié pour créer cotre compte)** \n'
            f'``` /requete```\n'
        )
        await message.channel.send(formatted_message)



    # elif message.content.startswith('🦧'):
    #     user = message.author
    #     myid = '<@274490061884751872>'
    #     for i in range (50):
    #         await message.channel.send(myid)
    #         await message.channel.send(myid)



client.run('MTIwMTgwMTY3NzMzODgzNzA0Mw.GG8mqm.a10DzZ8uUHGa46RuBZ7rcaJziyxULIwt514l6Q')
