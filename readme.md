# Hurl.ia - Bot Discord permettant l'interraction avec nu site

## Contexte
Hurlea est un site de streaming en développement, et j'avais besoin d'un system permettant de rendre possible l'inscription uniquement aux utilisateurs d'un serveur discord.
## Description
Ce bot Discord est conçu pour aider les utilisateurs à s'inscrire sur un site web et à soumettre des requêtes pour ajouter des films ou des séries sur le site.

## Fonctionnalités

### Enregistrement
- Utilisez la commande `/register` pour vous inscrire sur le site.
- Vous devez avoir le rôle "vérifié" pour créer votre compte.

![alt text](img/img.png)
- Si le compte est créé avec succès, un utilisateur est ajouté à la bdd du site avec toutes les informations nécessaires (pseudo, avatar, id_discord) un token est généré aléatoirement et sert de mdp temporaire.
Puis l'utilisateur reçoit un DM avec un lien de connexion ainsi que le mdp temporaire

![alt text](img/img_2.png)![alt text](img/img_3.png)![alt text](img/img_5.png)
- le nom d'utilisateur ainsi que l'avatar discord est recupéré et utilisé sur le site 

![alt text](img/img_12.png)

- suite à la connetion l'utilisateur est amené à changer son mdp
- il y une gestion d'erreur (si l'utilisateur est déjà inscrit notamment)

![alt text](img/img_2.png)![alt text](img/img_4.png)

### Requêtes TMDB
- Utilisez les commandes `/requete` ou `/req` pour soumettre une requête pour réclamer l'ajout d'un film ou d'une série sur le site.
- Vous devez avoir le rôle "vérifié" pour soumettre une requête.
- si l'utilisateur envoie `/requete` ou `/req` un message expliquant le fonctionnement de la requete

![alt text](img/img_6.png)
- si l'utilisateur envoie `/req https://www.themoviedb.org/...` avec un lien TMDB valide la requête est ajouté à la base de donnée

![alt text](img/img_7.png)
![alt text](img/img_8.png)
- Si l'utilisateur à déjà effectuer cette requête alors un message indiquant le problème est envoyé sur le discord

![alt text](img/img_9.png)
- Si l'utilisateur n'est pas inscrit sur les sites alors un message indiquant le problème est envoyé sur le discord

![alt text](img/img_10.png)
### Aide
- Utilisez la commande `/help` pour afficher les informations d'aide.
- Des instructions sont fournies pour l'enregistrement et la soumission de requêtes.

![alt text](img/img_11.png)
## Prérequis
- Discord.py
- Intégration avec un site web (actuellement configuré pour localhost:8000)

## Installation
1. Installez les dépendances : `pip install discord.py requests`
2. Exécutez le script : `python bot_discord.py`

N'oubliez pas de configurer les variables nécessaires telles que l'intégration avec le site web.


---
