import discord
import secrets
import re
import requests

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

def is_valid_tmdb_link(link):
    # Utilise une expression régulière pour vérifier si le lien commence par https://www.themoviedb.org/
    return re.match(r'^https://www.themoviedb.org/', link) is not None


@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('/register'):
        user = message.author
        username = user.name
        user_id = user.id
        user_avatar = user.display_avatar.with_size(128).url
        user_joined = user.joined_at

        verified_role = discord.utils.get(user.roles, name='vérifié')
        if verified_role:
            roles = [role.name for role in user.roles]
            token = secrets.token_urlsafe(16)
            url = f'127.0.0.1:8000/register/{token}'
            #
            # url2 = 'localhost://127.0.0.1:8000'
            # response = requests.post("localhost://127.0.0.1:8000/login",
            #                          data={"key": "value"},
            #                          headers={"Content-Type": "application/json"},
            #                          )
            # # Stocke le token en base de données
            # print(response.json())

            formatted_message = (
                f'>>> **Username:** {username}\n'
                f'**ID:** {user_id}\n'
                f'**Roles:** {", ".join(roles)}\n'
                f'**URL:** {url}\n'
                f'**Avatar:** {user_avatar}\n'
                f'**Joined Server on:** {user_joined}'
            )

            # Envoi des ces informations vers l'URL d'inscription en méthode POST...
            # puis création de l'utilisateur en bdd

            await message.channel.send(formatted_message)
            await message.channel.send(
                f'### Bonjour {username}, suivez ce lien: pour vous inscrire: http://{url} ###')


        else:
            await message.channel.send("Vous n'avez pas le rôle nécessaire.")
    elif message.content.startswith('/login'):

        user = message.author
        username = user.name
        id = user.id
        token = secrets.token_urlsafe(16)

        # verifie dans la bdd si l'utilisateur est dans la bdd grace a son id

        url = f'127.0.0.1:8000/login/{token}'

        # stocke le teken en bdd
        # Envoi de l'id et du token vers l'URL de connection en méthode POST...

        formatted_message = (
            f'>>> **Username:** {id}\n'
            f'**url de connection:** {url}\n'
        )

        await message.channel.send(formatted_message)
        await message.channel.send(
            f' ### Bonjour {username}, suivez ce lien: pour vous connecter: http://{url} ###')

    elif message.content.startswith('/requete'):
        content_parts = message.content.split()
        if len(content_parts) > 1:
            if is_valid_tmdb_link(content_parts[1]):
                await message.channel.send(f'Lien tmdb valide')
                # ajoute de la requete dans la bdd
            else:
                await message.channel.send(f'### il y une erreur dans ta requete, ###\ns'
                                           f'### Envoie un lien [tmdb](https://www.themoviedb.org/): ``` /requete https://www.themoviedb.org/... ``` ')
        else:
            await message.channel.send(

                f'### Quel film ou série désires-tu? ###\n'

                f'### Envoie un lien [tmdb](https://www.themoviedb.org/): ``` /requete https://www.themoviedb.org/... ``` ')

    elif message.content.startswith('/help'):
        formatted_message = (
            f'>>> **S\'inscrire sur le site:** \n'
            f'``` /register ```\n'

            f'**Se connecter sur le site:** \n'
            f'``` /login ```\n'

            f'**Demander à ajouté un film ou une série:** \n'
            f'``` /requete```\n'
        )
        await message.channel.send(formatted_message)



    # elif message.content.startswith('🦧'):
    #     for i in range (2):
    #         await message.channel.send("<@yamnyr> ")
    #

client.run('MTIwMTgwMTY3NzMzODgzNzA0Mw.GG8mqm.a10DzZ8uUHGa46RuBZ7rcaJziyxULIwt514l6Q')
